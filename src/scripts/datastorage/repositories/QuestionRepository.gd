extends Object

class_name QuestionRepository

func get_records() -> Array:
	
	var ctx = DbContext.new()
	
	ctx.db.query("select * from questions")
	
	var result := []

	for obj in ctx.db.query_result:
		var d: Dictionary = (obj as Dictionary)
		
		var q: Question = Question.new().from_dict(d)
		
		ctx.db.query(
			"select * from answers where answers.id_question = " + str(q.id)
		)
		
		for ans in ctx.db.query_result:
			q.answers.append(Answer.new().from_dict(ans))
		
		result.append(q)

	return result


func get_record(id: int) -> Question:
	
	var ctx = DbContext.new()
	
	ctx.db.query(
		"select * from questions where questions.id = " + str(id) + " limit 1;"
	)
	
	var q: Question
	
	if len(ctx.db.query_result) > 0:
		q = Question.new().from_dict(ctx.db.query_result[0])
		
		ctx.db.query(
			"select * from answers where answers.id_question = " + str(q.id)
		)
		
		for ans in ctx.db.query_result:
			q.answers.append(Answer.new().from_dict(ans))
	
	return q
