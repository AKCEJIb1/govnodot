extends Object

class_name AnswerRepository

func get_records() -> Array:
	
	var ctx = DbContext.new()
	
	ctx.db.query("select * from answers")
	
	var result := []
	
	for obj in ctx.db.query_result:
		result.append(Answer.new().from_dict(obj))
	
	return result
