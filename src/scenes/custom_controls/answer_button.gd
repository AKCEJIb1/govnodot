extends Button

class_name AnswerButton

var _answer: Answer

func _init(answer: Answer):
	_answer = answer
	text = _answer.title
