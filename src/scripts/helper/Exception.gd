extends Object
class_name Exception

static func throw(message: String):
	
	var stack := get_stack()
	var formatted_stack := ""

	for id in range(len(stack) - 1):
		formatted_stack += (
			"\t at %s(%s:%s)\n" % 
			[
				str(stack[id + 1]["function"]), 
				str(stack[id + 1]["source"]),
				str(stack[id + 1]["line"]),
			]
		)

	printerr("[Exception]: %s\n%s" % [message, formatted_stack])
	push_error("[Exception]: %s\n%s" % [message, formatted_stack])
	assert(false, "[Exception]: %s\n%s" % [message, formatted_stack])
