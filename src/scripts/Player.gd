class_name Player
extends Node

signal health_changed(value)
signal points_changed(value)

var health: int = 2 setget health_changed
var points: int = 0 setget points_changed

func health_changed(value: int):
	health = value
	emit_signal(get_stack()[0]["function"], value);

func points_changed(value: int):
	points = value
	emit_signal(get_stack()[0]["function"], value);
