extends Dataclass

class_name Answer

func _init().("Answer", {print_newline = true, sort_keys = false}): pass

var id: int
var title: String
var id_question: int
var is_correct: bool
