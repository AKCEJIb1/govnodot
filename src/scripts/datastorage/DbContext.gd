extends Reference
class_name DbContext

const SQlite = preload("res://addons/godot-sqlite/bin/gdsqlite.gdns")

var db: SQlite
var db_path_default = "res://db/questions.db"
var connection: bool

func _init(configuration: Dictionary = {}):
	db = SQlite.new()
	db.default_extension = ""
	db.read_only = (
		configuration["db_readonly"] if "db_readonly" in configuration 
		else true
	)
	db.foreign_keys = true
	db.path = (
		configuration["db_path"] if "db_path" in configuration 
		else db_path_default
	)
	connection = db.open_db()
	if not connection:
		Exception.throw("Cannot open database: %s" % db.path)

func free():
	.free()
	if connection:
		db.close_db()
