extends Node

var qr := QuestionRepository.new()

export(NodePath) var lbl_question_title_path
export(NodePath) var lbl_question_text_path
export(NodePath) var lbl_health_path
export(NodePath) var lbl_points_path

onready var lbl_question_title := get_node(lbl_question_title_path) as Label
onready var lbl_question_text := get_node(lbl_question_text_path) as Label
onready var lbl_health := get_node(lbl_health_path) as Label
onready var lbl_points := get_node(lbl_points_path) as Label

var questions: Array = []
var current_question_id := 0
var ply := Player.new()

func _ready():
	randomize()
	ply.connect("health_changed", self, "_update_health_lbl")
	ply.connect("points_changed", self, "_update_points_lbl")

	questions = qr.get_records()
	questions.shuffle()

	_show_question(current_question_id)
	_update_health_lbl(ply.health)
	_update_points_lbl(ply.points)

func _update_health_lbl(value):
	lbl_health.text = "Здоровье: %d" % value

func _update_points_lbl(value):
	lbl_points.text = "Очки: %d" % value

func _show_question(id: int):
	
	NodeUtils.clear_children(get_node("ui/buttons_area"))
	
	var fq = questions[id] as Question
	
	lbl_question_title.text = (
		"Вопрос (%d / %d): %s" % [
			current_question_id + 1, 
			len(questions), 
			fq.title
		]
	)
	lbl_question_text.text = fq.text
	
	var randomize_answers = fq.answers.duplicate(true)
	randomize_answers.shuffle()
	
	for answer in randomize_answers:
		_create_answer_button(answer)

func _create_answer_button(answer: Answer):
	var button = AnswerButton.new(answer)
	button.connect("pressed", self, "_button_pressed", [button])
	get_node("ui/buttons_area").add_child(button)

func _button_pressed(sender: AnswerButton):
	if sender._answer.is_correct:
		ply.points += 1
	else:
		ply.health -= 1

	if current_question_id < len(questions) - 1 and ply.health > 0:
		current_question_id += 1
		_show_question(current_question_id)
	else:
		if ply.health <= 0:
			SceneSwitcher.change_scene("res://scenes/MainScene.tscn", {
				"finish_type": "lose",
				"points": ply.points,
				"total": len(questions)
			})
		else:
			SceneSwitcher.change_scene("res://scenes/MainScene.tscn", {
				"finish_type": "won",
				"points": ply.points,
				"health": ply.health,
				"total": len(questions)
			})
