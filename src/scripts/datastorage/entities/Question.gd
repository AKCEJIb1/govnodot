extends Dataclass

class_name Question

func _init().("Question", {print_newline = true, sort_keys = false}): pass

var id: int
var title: String
var text: String
var answers: Array
