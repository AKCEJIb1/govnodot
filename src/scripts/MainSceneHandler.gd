extends Node

func _ready():
	var type = SceneSwitcher.get_param("finish_type")
	var health = SceneSwitcher.get_param("health")
	var points = SceneSwitcher.get_param("points")
	var total = SceneSwitcher.get_param("total")
	
	match type:
		"lose":
			get_node("StartGame").visible = false
			get_node("YouWon").visible = false
			get_node("YouLose").visible = true
			(get_node("YouLose/ui/GridContainer/lbl_info") as Label).text = (
				"Отвечено верно: %d из %d" % [points, total]
			)
		"won":
			get_node("StartGame").visible = false
			get_node("YouWon").visible = true
			get_node("YouLose").visible = false
			(get_node("YouWon/ui/GridContainer/lbl_info") as Label).text = (
				"Отвечено верно: %d из %d\nЗдоровья осталось: %d" % [points, total, health]
			)
		_:
			get_node("StartGame").visible = true
			get_node("YouWon").visible = false
			get_node("YouLose").visible = false

func _on_btn_start_pressed():
	get_tree().change_scene("res://scenes/QuestionScene.tscn")

func _on_btn_exit_pressed():
	get_tree().quit()

func _on_btn_restart_pressed():
	get_tree().change_scene("res://scenes/QuestionScene.tscn")
